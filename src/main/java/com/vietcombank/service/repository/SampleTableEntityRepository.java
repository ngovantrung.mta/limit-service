package com.vietcombank.service.repository;

import com.vietcombank.service.entity.SampleTableEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SampleTableEntityRepository extends JpaRepository<SampleTableEntity, UUID> {
}