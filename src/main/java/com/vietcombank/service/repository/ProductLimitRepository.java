
package com.vietcombank.service.repository;

import com.vietcombank.service.entity.ProductLimitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
public interface ProductLimitRepository extends JpaRepository<ProductLimitEntity, UUID> {
    
}
