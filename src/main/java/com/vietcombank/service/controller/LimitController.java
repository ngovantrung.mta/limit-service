package com.vietcombank.service.controller;

import com.vietcombank.service.entity.ProductLimitEntity;
import com.vietcombank.service.service.ProductLimitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/restapi")
public class LimitController {
@Autowired
ProductLimitService productlimitService;
@GetMapping("/getAllProductLimit")
 private List<ProductLimitEntity> getAllProductLimit()
  {  

      return productlimitService.getAllProductLimit();
  }  
  @GetMapping("/getProductLimits/{id}")
private ProductLimitEntity getProductLimits(@PathVariable("id") String id)
{  
  return productlimitService.getProductLimitById(id);
}  
@DeleteMapping("/deleteProductLimit/{id}")
private void deleteProductLimit(@PathVariable("id") String id)
{
    productlimitService.delete(id);
}  
@PostMapping("/saveProductLimit")
private void saveProductLimit(@RequestBody ProductLimitEntity  productLimits)
{  
  productlimitService.saveOrUpdate(productLimits);
  return;
}  
@PutMapping("/update")
private void update(@RequestBody ProductLimitEntity productLimits)
 {  
    productlimitService.saveOrUpdate(productLimits);
    return;
 }  
}
