package com.vietcombank.service.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.UUID;

@Entity
@Table(name = "limitbal")
public class ProductLimitEntity {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "procode")
    private String procode;

    @Column(name = "prolimit")
    private int prolimit;

    @Column(name = "prodes")
    private String prodes;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProcode() {
        return procode;
    }

    public void setProcode(String procode) {
        this.procode = procode;
    }

    public int getProlimit() {
        return prolimit;
    }

    public void setProlimit(int prolimit) {
        this.prolimit = prolimit;
    }

    public String getProdes() {
        return prodes;
    }

    public void setProdes(String prodes) {
        this.prodes = prodes;
    }
}
