package com.vietcombank.service.service;
import java.util.List;

import com.vietcombank.service.entity.ProductLimitEntity;
import com.vietcombank.service.repository.ProductLimitRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class ProductLimitService {
    @Autowired
    private ProductLimitRepository productLimitEntityRepository;

    //getting all books record by using the method findaAll() of CrudRepository  
public List<ProductLimitEntity> getAllProductLimit()
 {
  
  return productLimitEntityRepository.findAll();
 }
 public ProductLimitEntity getProductLimitById(String id)
  {  
   return productLimitEntityRepository.findById(UUID.fromString(id)).get();
  }  
  public void saveOrUpdate(ProductLimitEntity productlimits)
   {  
    productLimitEntityRepository.save(productlimits);

   }  
   public void delete(String id)
   {  
    productLimitEntityRepository.deleteById(UUID.fromString(id));
   }  
//updating a record  
 public void update(ProductLimitEntity ProductLimits, int id)
 {  
    productLimitEntityRepository.save(ProductLimits);
 }  
}
