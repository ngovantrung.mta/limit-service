--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE LIMITBAL
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    PROCODE   varchar(255),
    PROLIMIT int,
    PRODES varchar(400)
);